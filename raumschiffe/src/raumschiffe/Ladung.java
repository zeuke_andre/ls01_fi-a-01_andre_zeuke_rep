package raumschiffe;

public class Ladung {
//Aufgabe 1 Anlegen der Umgebung
	private String bezeichnung;	//Die Variable f�r die Bezeichnung der Ladung wird erstellt
	private int menge;			//Die Variable f�r die Menge der Ladung wird erstellt
//von zeile 8 bis 11 wird der Vollparametrisierte Konstruktor f�r Ladung erstellt
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
//Der Setter der Bezeichnung wird erstellt
	public void setBezeichnung(String bezeichnung2) {
		bezeichnung = bezeichnung2;
	}
//Der Getter der Bezeichnung wird erstellt
	public String getBezeichnung() {
		return this.bezeichnung;
	}
//Der Setter f�r die Menge wird erstellt
	public void setMenge(int menge2) {
		menge = menge2;

	}
//Der Getter f�r die Menge wird erstellt
	public int getMenge() {
		return this.menge;
	}
}
