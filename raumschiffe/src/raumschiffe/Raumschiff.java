package raumschiffe;

import java.util.ArrayList;
//Aufgabe 1 Anlegen der Ungebung
public class Raumschiff { //Start der Klasse Raumschiff

	private String nameSchiff;			//Anlegen der Variable f�r den Schiffsnamen
	private int androidenStueck;		//Anlegen der Variable f�r die Anzahl der Androiden
	private int lebenssysteminProzent;	//Anlegen der Variable f�r den Prozentualen stand der Lebenserhaltungssysteme
	private int huelleinProzent;		//Anlegen der Variable f�r den Prozentualen stand der h�lle
	private int schildinProzent;		//Anlegen der Variable f�r wie viel der St�rke der H�lle noch vorhanden sind
	private int energieinProzent;		//Anlegen der Variable f�r wie viel Energie �brig ist
	private int photonentorpedoAnzahl;	//Anlegen der Variable f�r die Anzahl der Photonentorpedos
	private static ArrayList<String> broadcastkommunikator = new ArrayList<String>();	//Der Broadcastkommunikator wurde erstellt
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();				//Das Ladungsverzeichnis wurde erstellt

	//in den zeilen 18 bis 20 habe ich einen Parameterlosen Konstruktor erstellt
	public Raumschiff() {

	}
//in den folgenden Zeilen von zeile 22 bis 32 sehen Sie den Vollst�ndigen Konstruktor f�r die Klassen
	public Raumschiff(String nameSchiff, int androidenStueck, int lebenssysteminProzent, int huelleinProzent,
			int schildinProzent, int energieinProzent, int photonentorpedoAnzahl) {
		this.nameSchiff = nameSchiff;
		this.androidenStueck = androidenStueck;
		this.lebenssysteminProzent = lebenssysteminProzent;
		this.huelleinProzent = huelleinProzent;
		this.schildinProzent = schildinProzent;
		this.energieinProzent = energieinProzent;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;

	} 
	//f�r jede Variable wird ein Getter und ein Setter erstellt
	
	//Erstellung eines Setters f�r den Schiffsname
	public void setNameSchiff(String nameSchiff2) {
		nameSchiff = nameSchiff2;
	}
	//Erstellung eines Getters f�r den Schiffsname
	public String getNameSchiff() {
		return this.nameSchiff;
	}

	public void setAndroidenStueck(int androidenSt�ck2) {
		androidenStueck = androidenSt�ck2;

	}

	public int getAndroidenstueck() {
		return this.androidenStueck;
	}

	public void setLebenssysteminProzent(int lebenssysteminProzent2) {
		lebenssysteminProzent = lebenssysteminProzent2;
	}

	public int getLebenssysteminProzent() {
		return this.lebenssysteminProzent;
	}

	public void setHuelleinProzent(int huelleinProzent2) {
		huelleinProzent = huelleinProzent2;
	}

	public int getHuelleinProzent() {
		return this.huelleinProzent;
	}

	public void setSchildinProzent(int schildinProzent2) {
		schildinProzent = schildinProzent2;
	}

	public int getSchildinProzent() {
		return this.schildinProzent;
	}

	public void setEnergieinProzent(int energieinProzent2) {
		energieinProzent = energieinProzent2;
	}

	public int getEnergieinProzent() {
		return this.energieinProzent;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl2) {
		photonentorpedoAnzahl = photonentorpedoAnzahl2;
	}

	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}//ende der Erstellung von Getter und Setter(alle Getter und Setter wurden Erstellt)
	//die Methode addLadung wird Erstellt
	public void addLadung(Ladung neueLadung) {
	ladungsverzeichnis.add(neueLadung);
	}
	
	//Aufgabe 2 Der Zustand des Raumschiffes ausgegeben
	//von zeile 99 bis zeile 107 wird die Methode f�r die Aufgabe 2 erstellt
	public void zustandRaumschiff() {
		System.out.println("Zustand des Raumschiffes " + this.getNameSchiff() + " :" +
				"\nAnzahl der Photonentopedos: " + this.getPhotonentorpedoAnzahl() +
				"\nZustand der Energieversorgung: " + this.getHuelleinProzent() +
				"\nZustand des Raumschiff: " + this.getSchildinProzent() + 
				"\nZustand der Lebenssysteme: " + this.getLebenssysteminProzent() +
				"\nZustand der Energieversorgung: " + this.getEnergieinProzent() +
				"\nAnzahl der Androiden: " + this.getAndroidenstueck());
	}//ende der Methode zustandRaumschiff
	
	//Aufgabe 3 Ladungsverzeichnis ausgeben
	//Anfang der Methode zur Ausgabe des Ladungs verzeichnis
	//wird im sp�teren verlauf daf�r Ben�tigt um herrauszufinden welche Ladungen alle auf dem Ausgew�hlten Schiff vorhanden sind 
	public void ladungsverzeichnisAusgeben() {
		System.out.println("Das Raumschiff " + this.getNameSchiff() + " enth�lt folgende Ladungen:");
		for(Ladung tmp:ladungsverzeichnis) {
			System.out.println(tmp.getBezeichnung() + " " + tmp.getMenge());
		}
	}//Ende der Methide LadungsverzeichnisAusgeben
	
	//Aufgabe 4 Photonentorpedos abschie�en
	//Die folgenden zeilen werden Ben�tigt damit das Raumschiff einen Torpedo abschie�en kann falls dies nicht m�glich ist wird die nachricht -=*click*=- ausgegeben
		public void photonentorpedosAbschiessen(Raumschiff r){
			if( getPhotonentorpedoAnzahl() <= 0) {
				nachrichtAnAlle("-=*Click*=-\n");}//methode nachrichtAnAlle wird aufgerufen
			else {
				setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl()-1);
				nachrichtAnAlle("Photonentopedo abgeschossen\n");
				treffer(r);//Methode treffer wird aufgerufen
			}
		}//ende der Methode photonentorpedosAbschiessen
		
		//Aufgabe 6 Treffer vermerken
		//Diese methode wird daf�r genutzt um einen treffer zu vermerken
		private void treffer(Raumschiff r){
			System.out.println(this.getNameSchiff() + "wurde getroffen!");
		}//ende der methode treffer
		
		//Aufgabe 5 Phaserkanone abschie�en
		//genauso wir bei der Aufgabe 4 wird diese Methode benutzt um etwas abzuschie�en doch im gegensatz zu dieser Aufgabe wird hier die Phaserkanone abgescho�en
		public void phaserkanoneSchiessen(Raumschiff r) {
			if (getEnergieinProzent() < 50 ) {
				nachrichtAnAlle("-=*Click*=-\n");//methode nachrichtAnAlle wird aufgerufen
			}else {
				setEnergieinProzent(getEnergieinProzent() - 50);
				nachrichtAnAlle("Phaserkanone abgeschossen");
				treffer(r);//Methode treffer wird aufgerufen
			}
		}//ende der Methode phaserkanoneAbschiessen
		
		//Aufgabe 7 Nachricht an Alle 
		//diese Methode wird daf�r genutzt um eine Nachricht an Alle Raumschffe zu senden
		public void nachrichtAnAlle(String message ){
			broadcastkommunikator.add(message);
		}//ende der Methode nachrichtAnAlle
		
		//Aufgabe 8 Logbuch Eintr�ge werden hierzur�ckgeben
		public static ArrayList<String> LogbuchEintrag(){
			return broadcastkommunikator;
		}//ende der Methode LogbuchEintrag
		
		//Aufgabe 9 Treffer vermerken
		//Der Treffer wird vermerkt in dem die Sch�sse immer mehr Schaden anrichten
		private void trefferVermerken(Raumschiff r) {
		r.setSchildinProzent(r.getSchildinProzent()-50);
		if(r.getSchildinProzent() <= 0) {
			r.setHuelleinProzent(r.getHuelleinProzent()-50);
			if(r.getHuelleinProzent() <= 0) {
				r.setLebenssysteminProzent(r.getLebenssysteminProzent()-50);
				nachrichtAnAlle("Lebenserhaltungssysteme worden vernichtet");
			}
		}
		}//ende der Methode Treffer vermerken
	}// ende der Klasse Raumschiff
