package raumschiffe;

public class Test {

	public static void main(String[] args) {
		//Raumschiff der Klingonen wird mit den in der Klammer stehende werten erstellt
		Raumschiff klingonen = new Raumschiff("IKS Hegh�ta", 2, 100, 100, 100, 100, 1);
		
		//Die Ladungen der klingonen werden erstellt
		Ladung ladungk1 = new Ladung("Ferengi Schneckensaft", 200);
		
		Ladung ladungk2 = new Ladung("Bat�leth Klingonen Schwert", 200);
		
		//Die Ladungen der klingonen werden dem Verzeichnis hinzugef�gt
		klingonen.addLadung(ladungk1);
		
		klingonen.addLadung(ladungk2);
		
		
		
		
		//Raumschiff der romulaner wird mit den in der Klammer stehende werten erstellt
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 2, 100, 100, 100, 100, 2);
		
		//Die Ladungen der Romulaner werden erstellt
		Ladung ladungr1 = new Ladung("Borg-Schrott", 5);
		
		Ladung ladungr2 = new Ladung("Rote Materie", 2);
		
		Ladung ladungr3 = new Ladung("Plasma-Waffe",50);
		
		//Die Ladungen der romulaner werden dem Verzeichnis hinzugef�gt
		romulaner.addLadung(ladungr1);
		
		romulaner.addLadung(ladungr2);
		
		romulaner.addLadung(ladungr3);
		
		
		
		
		//Raumschiff der vulkanier wird mit den in der Klammer stehende werten erstellt
		Raumschiff vulkanier = new Raumschiff("NiVar", 2, 100, 50, 80, 80, 0);
		
		//Die Ladungen der vulkanier werden erstellt
		Ladung ladungv1 = new Ladung("Forschungssonde", 35);
		
		Ladung ladungv2 = new Ladung("Photonentorpedo", 3);
		
		//Die Ladungen der Vulkanier werden dem Verzeichnis hinzugef�gt
		vulkanier.addLadung(ladungv1);
		
		vulkanier.addLadung(ladungv2);
		
		
		//Die folgenden Methoden werden wie in der Aufgabe gew�nscht aufgerufen
	klingonen.photonentorpedosAbschiessen(romulaner);
	romulaner.phaserkanoneSchiessen(klingonen);
	vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
	klingonen.zustandRaumschiff();
	klingonen.ladungsverzeichnisAusgeben();
	klingonen.photonentorpedosAbschiessen(romulaner);
	klingonen.photonentorpedosAbschiessen(romulaner);
	klingonen.zustandRaumschiff();
	vulkanier.zustandRaumschiff();
	romulaner.zustandRaumschiff();
	klingonen.ladungsverzeichnisAusgeben();
	vulkanier.ladungsverzeichnisAusgeben();
	romulaner.ladungsverzeichnisAusgeben();
	System.out.println(Raumschiff.LogbuchEintrag());
	
	}

}
