﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
      
    	
    	while(true) {//dauerschleife für den Automaten
    	zuZahlenderBetrag = fahrkartenbestellung();//gebe Methode mit Fahrkarten liste aus und erfasse länge der Fahrkarten
    	
    	
    	eingezahlterGesamtbetrag = fahrkartenBezahlen();//Methode zum Bezahlen von Fahrkarten
       
    	fahrkartenAusgeben();//gebe Fahrkarten aus
    	
    	rückgabebetrag = rueckgeldAusgeben();//gebe rückgeld aus
      

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.\n");
    	}
    }
    static Scanner tastatur = new Scanner(System.in);//Variable für input
    static double zuZahlenderBetrag; //variable für Kosten
    static double eingezahlterGesamtbetrag; //variable für den Betrag der eingezahlt wurde
    static double eingeworfeneMünze;//variable zur aufnahme der eingeworfenen Münzen
    static double rückgabebetrag;// variable für das Rückgeld
    static byte AnzahlderTickets = 0; 
    
    public static double fahrkartenbestellung() {  	//beginn der Methode fahrkartenbestellung
    	        String[] Fahrscheine = {"Einzelfahrschein Berlin AB",
    	                "Einzelfahrschein Berlin BC",
    	                "Einzelfahrschein Berlin ABC",
    	                "Kurzstrecke",
    	                "Tageskarte Berlin AB",
    	                "Tageskarte Berlin BC",
    	                "Tageskarte Berlin ABC",
    	                "Kleingruppen-Tageskarte Berlin AB",
    	                "Kleingruppen-Tageskarte Berlin BC",
    	                "Kleingruppen-Tageskarte Berlin ABC"};

    	        double[] Fahrscheinpreise = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};

    	        boolean pay = false;
    	        byte tickets = 0; // (byte)Variable, Operationen: User-Input für die Anzahl der Tickets, zuzahlenderBetrag wird mit den tickets multipliziert, um den zuzahlenden  betrag aller tickets hat
    	        // ich habe mich für den Datentyp byte entschieden, das der nicht in den negativen bereich gehen kann, was in den fall sinnvoll ist
    	        double zuZahlenderBetrag = 0; // (double)Variable, Operationen: User-Inut, Multiplikation mit den tickets, While loop welcher den restbetrag bestimmt...
    	        Scanner tastatur = new Scanner(System.in); // tastatur ist eie Variable

    	        while(!pay) { // wenn pay nicht tue ist läuft die schleife
    	            System.out.println("\nFahrkartenbestellvorgang");
    	            System.out.printf("Zwischensumme: %.2f€ \n", zuZahlenderBetrag);
    	            System.out.println("========================");
    	            System.out.println("Wählen sie:");
    	            for (int i = 0; i<Fahrscheinpreise.length; i++) {
    	                System.out.printf("|- %s | %s (%.2f€)\n", i, Fahrscheine[i], Fahrscheinpreise[i]);
    	            }
    	            System.out.printf("|- %s | Bezahlen\n", Fahrscheinpreise.length);
    	            System.out.print("|- Ihre Wahl:");
    	            int choice = tastatur.nextInt();

    	            if(choice == Fahrscheinpreise.length){ // Überprüfen ob eingabe der Zahl zu Bezhalen entspricht ist (Bezahlen)
    	                pay = true;
    	                System.out.printf("Sie müssen %.2f EURO bezahlen. \n", zuZahlenderBetrag);
    	            }
    	            else if(choice > 0 && choice < Fahrscheinpreise.length) { // Überprüfen ob eingabe gültig ist
    	                tickets = 0;
    	                while(tickets < 1 || tickets > 10) {
    	                    System.out.printf("Wie viele %s wünschen sie (1 -10): ", Fahrscheine[choice]);
    	                    tickets = tastatur.nextByte();
    	                    zuZahlenderBetrag = zuZahlenderBetrag + (tickets* Fahrscheinpreise[choice]);
    	                }
    	            }
    	            else {System.out.printf("Sie trafen eine ungültige Eingabe. Bitte wählen sie eine Zahl zwischen %s und %s.", 0, Fahrscheinpreise.length);
    	            }
    	        }
    	        return zuZahlenderBetrag;
    	    }

    	 	  
       

    
    
    public static double fahrkartenBezahlen() {//Methode zum Bezahlen der Fahrkarte
    	 
    	eingezahlterGesamtbetrag = 0.0;
         while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
         {
      	   System.out.printf("Noch zu zahlen: %.2f EURO\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
      	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      	   eingeworfeneMünze = tastatur.nextDouble();
      	   if(eingeworfeneMünze <= 2 && eingeworfeneMünze >= 0.5){
      	   
             eingezahlterGesamtbetrag += eingeworfeneMünze;
             }
             else {
            	 System.out.println("Sie dürfen nur mind. 5Ct, höchstens 2 Euro einwerfen ");
      	   }
         }
         return eingezahlterGesamtbetrag;
    }
    
    
          public static void fahrkartenAusgeben()  {//Methode zur Fahrkarten ausgabe
        	  
        	  System.out.println("\nFahrschein wird ausgegeben");
              for (int i = 0; i < 8; i++)
              {
                 System.out.print("=");
                 try {
       			Thread.sleep(250);
       		} catch (InterruptedException e) {
       			// TODO Auto-generated catch block
       			e.printStackTrace();
       		}
              }
              System.out.println("\n\n");
        	  
          }
          
          
          
          public static double rueckgeldAusgeben() {//Methode zur Rückgeldausgabe
        	  rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
              if(rückgabebetrag > 0.0)
              {
           	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
           	   System.out.println("wird in folgenden Münzen ausgezahlt:");
  
                  while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
                  {
               	  System.out.println("2 EURO");
       	          rückgabebetrag -= 2.0;
                  }
                  while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
                  {
               	  System.out.println("1 EURO");
       	          rückgabebetrag -= 1.0;
                  }
                  while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
                  {
               	  System.out.println("50 CENT");
       	          rückgabebetrag -= 0.5;
                  }
                  while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
                  {
               	  System.out.println("20 CENT");
        	          rückgabebetrag -= 0.2;
                  }
                  while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
                  {
               	  System.out.println("10 CENT");
       	          rückgabebetrag -= 0.1;
                  }
                  while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
                  {
               	  System.out.println("5 CENT");
        	          rückgabebetrag -= 0.05;
                  }
              } 
              return rückgabebetrag;
          }
          }
         

