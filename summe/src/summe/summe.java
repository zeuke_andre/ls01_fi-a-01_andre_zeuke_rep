
package summe;
import java.util.Scanner;

public class summe {

	public static void main(String[] args) {
			
		Scanner sc = new Scanner(System.in);
		int summBis = 0;
		int summe = 0;
		int counter = 0;
		
		System.out.print("Geben Sie eine Zahl ein, bis welche Sie addieren wollen: ");
		summBis = sc.nextInt();
		
		//Kopfschleife
		while(counter <= summBis) {
			summe = summe + counter;
			counter ++;
			
		}
		
		System.out.printf("\nMit der Kopfgesteuerten: %d ",summe);
		
		//Fu�gesteuert
		summe = 0;
		counter = 0;
		
		do {
			summe = summe + counter;
			counter = counter + 1;
					
		}while(counter <= summBis);
		
		System.out.printf("\nMit der Kopfgesteuerten: %d ",summe);
	}

}
